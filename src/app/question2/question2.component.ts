import { Component, OnInit } from '@angular/core';
import { DataApiService } from '../services/data-api.service';
import { Preg2 } from '../models/preg2-interface';

@Component({
  selector: 'app-question2',
  templateUrl: './question2.component.html',
  styleUrls: ['./question2.component.css']
})
export class Question2Component implements OnInit {

  constructor(private dataApiService: DataApiService) { }
  private quest2: Preg2;
  pageActual: number = 1;
  public myCounter: number = 0;
  ngOnInit() {
    this.getQuestion2();
  }
getQuestion2(): void {
    this.dataApiService
      . getPregunta2()
      .subscribe((quest2: any) => (this.quest2 = quest2));
  }
}
