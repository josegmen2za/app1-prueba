import { Preg3 } from '../models/preg3-interface';
import { Preg2 } from '../models/preg2-interface';
import { Preg1 } from '../models/preg1-interface';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/operators';



@Injectable({
  providedIn: 'root'
})
export class DataApiService {
  constructor(private http: HttpClient) { }

  public selectedPreg1: Preg1 = {
    userId: '',
    id: '',
    title: '',
    body: ''
  };
  public selectedPreg2: Preg2 = {
    id: '',
    name: '',
    username: '',
    company: ''
  };
public selectedPreg3: Preg3 = {
  data: '',
  error: '',
  success: ''
  };



  getPregunta1() {
    // tslint:disable-next-line: variable-name
    const url_api1 = `https://jsonplaceholder.typicode.com/posts`;
    return this.http.get(url_api1);
  }
  getPregunta2() {
    // tslint:disable-next-line: variable-name
    const url_api2 = `https://jsonplaceholder.typicode.com/users`;
    return this.http.get(url_api2);
  }
  getPregunta3() {
    // tslint:disable-next-line: variable-name
    const url_api3 = `http://168.232.165.184/prueba/dist`;
    return this.http.get(url_api3);
  }
}
