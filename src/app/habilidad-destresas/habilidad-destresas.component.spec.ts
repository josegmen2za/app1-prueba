import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HabilidadDestresasComponent } from './habilidad-destresas.component';

describe('HabilidadDestresasComponent', () => {
  let component: HabilidadDestresasComponent;
  let fixture: ComponentFixture<HabilidadDestresasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HabilidadDestresasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HabilidadDestresasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
