import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TrabajosComponent } from './trabajos/trabajos.component';
import { EstudiosComponent } from './estudios/estudios.component';
import { NavbarComponent } from './navbar/navbar.component';
import { InicioComponent } from './inicio/inicio.component';
import { ContactoComponent } from './contacto/contacto.component';
import { OrbitComponent } from './orbit/orbit.component';
import { PillarComponent } from './pillar/pillar.component';
import { HabilidadDestresasComponent } from './habilidad-destresas/habilidad-destresas.component';
import { PaginaErrorComponent } from './pagina-error/pagina-error.component';
import { FooterComponent } from './footer/footer.component';
import { BannerComponent } from './banner/banner.component';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { Question1Component } from './question1/question1.component';
import { Question2Component } from './question2/question2.component';
import { Question3Component } from './question3/question3.component';

// Services
import { DataApiService } from '../app/services/data-api.service';

// Externals
import { NgxSpinnerModule } from 'ngx-spinner';
import { NgxPaginationModule } from 'ngx-pagination';


@NgModule({
  declarations: [
    AppComponent,
    TrabajosComponent,
    EstudiosComponent,
    NavbarComponent,
    InicioComponent,
    ContactoComponent,
    OrbitComponent,
    PillarComponent,
    HabilidadDestresasComponent,
    PaginaErrorComponent,
    FooterComponent,
    BannerComponent,
    Question1Component,
    Question2Component,
    Question3Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    NgxSpinnerModule,
    NgxPaginationModule
  ],
  providers: [{provide: LocationStrategy, useClass: HashLocationStrategy, }, [DataApiService],],
  bootstrap: [AppComponent]

})
export class AppModule { }
