import {Component, ViewChild, ElementRef, OnInit } from '@angular/core';
declare var jQuery: any;
declare var $: any;
@Component({
  selector: 'app-estudios',
  templateUrl: './estudios.component.html',
  styleUrls: ['./estudios.component.css']
})
export class EstudiosComponent implements OnInit {
@ViewChild('dataTable') table: { nativeElement: any; };
  dataTable: any;
  dtOption: any = {};

  title = 'men2za-app';



  constructor() { }

  ngOnInit() {
    this.dtOption = {
        "paging":   true,
        "ordering": true,
        "info":     true,
        "columnDefs": [
          {
              "targets": [ 2 ],
              "visible": true,
              "searchable": true
          },
          {
              "targets": [ 3 ],
              "visible": true
          }
      ],
      "pagingType": "full_numbers"
    };
    this.dataTable = $(this.table.nativeElement);
    this.dataTable.DataTable(this.dtOption);
  }

}
