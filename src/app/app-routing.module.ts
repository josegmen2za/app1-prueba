import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';

import { TrabajosComponent } from './trabajos/trabajos.component';
import { EstudiosComponent } from './estudios/estudios.component';
import { InicioComponent } from './inicio/inicio.component';
import { ContactoComponent } from './contacto/contacto.component';
import { OrbitComponent } from './orbit/orbit.component';
import { PillarComponent } from './pillar/pillar.component';
import { HabilidadDestresasComponent } from './habilidad-destresas/habilidad-destresas.component';
import { PaginaErrorComponent } from './pagina-error/pagina-error.component';
import { BannerComponent } from './banner/banner.component';
import { Question1Component } from '../app/question1/question1.component';
import { Question2Component } from '../app/question2/question2.component';
import { Question3Component } from '../app/question3/question3.component';

const routes: Routes = [
  { path: 'contacto', component: ContactoComponent },
  { path: 'orbit', component: OrbitComponent },
  { path: 'pillar', component: PillarComponent },
  { path: 'inicio', component: InicioComponent },
  { path: 'banner', component: BannerComponent },
  { path: 'estudios', component: EstudiosComponent },
  { path: 'habilid-destresas', component: HabilidadDestresasComponent },
  { path: 'pagina-error', component: PaginaErrorComponent },
  { path: 'inicio', component: InicioComponent },
  { path: 'question1', component: Question1Component }, // TODO: only users auth
  { path: 'question2', component: Question2Component }, // TODO: only users auth
  { path: 'question3', component: Question3Component }, // TODO: only users auth
  { path: 'trabajos', component: TrabajosComponent },
  { path: '', component:  InicioComponent, pathMatch: 'full' },
  { path: '**', redirectTo: '/', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
