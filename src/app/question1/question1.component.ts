import { Component, OnInit, ViewChild } from '@angular/core';
import { DataApiService } from '../services/data-api.service';
import { Preg1 } from '../models/preg1-interface';
import { NgForm } from '@angular/forms';
import { NgIf } from '@angular/common';
declare var jQuery: any;
declare var $: any;

@Component({
  selector: 'app-question1',
  templateUrl: './question1.component.html',
  styleUrls: ['./question1.component.css'],
})
export class Question1Component implements OnInit {


  constructor(private dataApiService: DataApiService) { }
  private quest1: Preg1;
  pageActual: number = 1;
  public myCounter: number = 0;

  ngOnInit() {
    this.getQuestion1();
  }
getQuestion1(): void {
    this.dataApiService
      . getPregunta1()
      .subscribe((quest1: any) => (this.quest1 = quest1));
  }
}
