import { Component, OnInit } from '@angular/core';
import { DataApiService } from '../services/data-api.service';
import { Preg3 } from '../models/preg3-interface';

@Component({
  selector: 'app-question3',
  templateUrl: './question3.component.html',
  styleUrls: ['./question3.component.css']
})
export class Question3Component implements OnInit {

  constructor(private dataApiService: DataApiService) { }
  private quest3: Preg3;
  pageActual: number = 1;
  public myCounter: number = 0;
  ngOnInit() {
    this.getQuestion3();
  }
getQuestion3(): void {
    this.dataApiService
      . getPregunta3()
      .subscribe((quest3: any) => (this.quest3 = quest3));
  }
}
